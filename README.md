# Fuck Fontesk

<strong>[EN]</strong>

Fontesk is a wonderful website that presents free and (sometimes) open-source fonts, taken from all over the net. They filter Github and Gitlab (platforms that are intended for project development) and publish any finished or in-progress font that they find. Thing is, when a type designer asked Fontesk admins to take her unreleased font files from their server, they blocked her so she can't access their website. 

Fuck Fontesk is an experimental protest font made in a few hours, because, why not.

## Specimen

![specimen1](documentation/specimen/fuckfontesk-specimen-01.png)

## License

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository follows the Unified Font Repository v2.0, a standard way to organize font project source files. Learn more at https://github.com/raphaelbastide/Unified-Font-Repository
